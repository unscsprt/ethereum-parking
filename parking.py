import serpent
from pyethereum import transactions, blocks, processblock, utils
import time

def print_balance_information(contract_addr, parking_owner, drivers):
	print ""
	print "The parking meter has " + genesis.account_to_dict(contract_addr)["balance"]
	print "The parking owner has " + genesis.account_to_dict(parking_owner)["balance"]
	for i in range(len(drivers)):
		print "Person "+str(i)+" has " + genesis.account_to_dict(drivers[i])["balance"]
	print ""


# Compile the seperent contract code (above)
code = serpent.compile(open('parking.se').read())

parking_owner_key = utils.sha3("parking_owner")
parking_owner = utils.privtoaddr(parking_owner_key)
person1_key = utils.sha3("person1")
person1 = utils.privtoaddr(person1_key)
person2_key = utils.sha3("person2")
person2 = utils.privtoaddr(person2_key)

genesis = blocks.genesis({parking_owner:10**18, person1:10**18, person2:10**18})

drivers = [person1, person2]

# Initialize the contract
tx = transactions.contract(genesis.get_nonce(parking_owner), 10**12, 10000, 0, code).sign(parking_owner_key)
success, contract_addr = processblock.apply_transaction(genesis,tx)

print_balance_information(contract_addr,parking_owner,drivers)

# Person 1 buys 30 seconds of time
tx1 = transactions.Transaction(genesis.get_nonce(person1), 1000, 10000, contract_addr, (10**13)/3, serpent.encode_datalist([int(time.time())])).sign(person1_key) 
s1, r1 = processblock.apply_transaction(genesis, tx1)

print "Person 1 is reserving the space for 10 seconds"
print_balance_information(contract_addr,parking_owner,drivers)

# Person 2 tries to buy 10 seconds of time (should fail)
tx2 = transactions.Transaction(genesis.get_nonce(person2), 1000, 10000, contract_addr, (10**13)/3, serpent.encode_datalist([int(time.time())])).sign(person2_key)
s2, r2 = processblock.apply_transaction(genesis, tx2)

print "Person 2 is attempting to reserve the space while Person 1 has it"
print_balance_information(contract_addr,parking_owner,drivers)

# Waits for 10 seconds
print "Person 2 is waiting for 10 seconds"
time.sleep(10)
tx3 = transactions.Transaction(genesis.get_nonce(person2), 1000, 10000, contract_addr, (10**13)/3, serpent.encode_datalist([int(time.time())])).sign(person2_key)
s3, r3 = processblock.apply_transaction(genesis, tx3)

print "Person 2 is reserving the space for 10 seconds"
print_balance_information(contract_addr,parking_owner,drivers)

# Parking owner claims his/her money
print "Parking owner is claiming money from parking meter"
tx4 = transactions.Transaction(genesis.get_nonce(parking_owner), 1000, 10000, contract_addr, 0, serpent.encode_datalist([])).sign(parking_owner_key)
s4, r4 = processblock.apply_transaction(genesis, tx4)
print_balance_information(contract_addr,parking_owner,drivers)